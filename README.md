# Manually Updating Production Data

This document should simply state, *"don't do it."* 

The reality is there are *exceptional* times when we need to resolve an *important* problem *expeditiously* and executing a query in production accomplishes that. It's important that database administrators follow the procedures outlined in this document to ensure data and systems security.

### Setup

*The ideal number of users with direct access to a production database is 2 or 3 administrators who never need to use their access.* 

The database administrator must follow the *principle of least privilege* when granting permissions to a production system. Direct database access shall not be given to anyone outside of the IT team, even if the access is read-only. Next to the possibility of data exfiltration and information exposure, a common compromise in the CIA triad of security is system availability. If you have a number of business users running ad-hoc queries on a production database, those queries may cause unexpected spikes in resource consumption and cause the production system to underperform or grind to a halt (as more and more users and downstream systems queue to retry their access repeatedly).

When an administrator provides access to a privileged user needing both read and write access to a database, it is safer to create two separate accounts for the user; a read-only account and a write-enabled account. This separation can prevent mistakes when using a database client application which can easily allow for accidental updates. If a database user forgets and is inconvenienced by not being able to make an update to production data using their read-only account, they will be required to switch connections; this is a reminder that they are switching to a privileged context with important responsibilities. It is up to the administrator to educate the privileged user on when to use the two separate accounts (the read-only account by default and only the write-enabled account when absolutely necessary) and it's up to the user to follow this self-disciplined practice. 

If only one write-enabled account is possible (e.g. for an iSeries DB2 user), the administrator should instruct the user to create two separate accounts and utilize a client read-only control in the database client. 

DBeaver's general connection settings can be configured for "read-only" even when the database server's user grants permit write operations.

![dbeaver-read-only](images/dbeaver-read-only.jpg)

Organize your connections so that Production (write-enabled) connections are in a separate folder from your default read-only connections. Placing production connections an extra step away will help keep you from using your most privileged accounts by default.

![dbeaver-prod-less-accessible](images/dbeaver-prod-less-accessible.jpg)

Identify all Production connection types and highlight them red. This will alert you to the dangers of working with production data. It's also ugly and jarring; you will want to collapse/hide this directory when it's not in use.

![dbeaver-prod-labeled](images/dbeaver-prod-labeled.jpg)

Be sure that the settings for production connection types do not use auto-commit, requires confirmation of SQL execution, and requires confirmation of data changes.

![dbeaver-setting-commit](images/dbeaver-setting-commit.jpg)

With this configuration for production connections, we have visual indications and three additional steps that will interrupt our second nature of making data changes on a whim.

![production-change-commit](images/production-change-commit.gif)

1. Production connections now have to be expanded and are highlighted red
2. Changes to data have to be confirmed
   - The generated SQL is previewed
   - The result is saved and can be previewed, but not committed.
   - The number of affected rows is displayed
3. The update does not actually occur until the transaction completes with a 'Commit'

### Consideration

Ask yourself, *"Do I really need to update the production data?"*

What are the alternative options?

- Fix the bug causing malformed data
- Schedule a deployed migration (scripted or written query) of the data after testing.
- Develop access to change the data by a privileged user (if this a recurring task and a bugfix is not applicable)
- Require the end-user to perform rework (if possible and acceptable).

The general criteria for changing production data manually is:

**Frequency:**  Seldom
**Importance:** High
**Timing:** Immediate

### Procedure:

1. Preparation & Mindfulness
2. Backup the data in scope
3. Re-check everything
4. Communicate the change
5. Review everything
6. Execute
7. Verify

#### 1 . Preparation & Mindfulness

Your heart rate should increase a little bit when you're connected to a production database and working with production data. You need to be careful. Take your time to make sure no mistakes are made, but always plan for failure anyway.  You're working on a living and breathing system. Is it feasible to stop systems and jobs that perform queries while the update is performed?

#### 2. Backup the data in scope

You can do one of the following:

- Backup the entire database
- Backup only the table(s) involved in the update
- Backup only the scoped data that will be changing

The more 'scoped' you're being with your backup data, the more experienced and mindful you should be of the changes you're making. Assess the risk and be certain that you will not accidentally make unrecoverable changes. Sometimes it's not feasible to backup and entire database because of its size and the time it would take. In rare cases, a table may be too large to back up entirely as well.

Verify your backup data! I've seen database clients produce bad statements, miss escape characters (producing invalid SQL), or simply stop execution of the dump after several lines and not report an error. 

#### 3. Re-check everything

In the following example, my goal will be to use data from `order_payments` and write the corresponding amount to the `curbstone_payments` table which currently shows zero dollar amounts.

![curbstone_payments_table](images/curbstone_payments_table.jpg)

![order-payments-table](images/order-payments-table.jpg)


I'm going to construct an update query that I think should work. But I'm not going to run it yet.

![1-initial-update](images/1-initial-update.jpg)

Instead, I'm going turn the `update` query into a `select` query. 

![2-update-turned-select](images/2-update-turned-select.jpg)

This will allow me to preview the data I will be updating. This new query will also give me a quick way to verify the data after the update query runs.

![data-update-to-select](images/data-update-to-select.jpg)

#### 4. Communicate the change

If the change is simple and the data is not highly sensitive, communicate the change as a message to the team or project manager. This provides a record of the previous and new state without a database backup while communicating what is being done. If the information needed to be reverted, another database administrator could act on this email to revert the change.

> Hi Becci,
>
> The order (id=89317) has been exported. It looks like the address (id=10129) name was too long and a non-ascii character needed to be removed.
>
> I changed "Art Exposé, Colorado Mesa University" to "Art Expose, CMU"
>
> Thanks,
>
> -DbAdmin

If the change is more complex or contains sensitive data, you will need to perform a backup of the data as described in step 2. 

#### 5. Review Everything

Are you confident in your query (accuracy, performance/timing)? Are you confident in your backup? Are you confident in your plan to restore data from a backup with no (or minimal) data loss? Check and re-check. Ask yourself, "What can go wrong?" and "What will I do when that happens?"

#### 6. Execute

When you're ready, perform and monitor the update.

#### 7. Verify

Once execution takes place, the clock starts for potential data loss. The sooner you decide whether you need to rollback or not,  the less chance there is for data loss. Do you need to rollback the changes that were made? If the systems are still operational, the data you just changed could be actively changed by users of the system.

In the event that a mistake was made when updating production data, you will need to revert the changes that were made using the backup. If there is a risk of operational activity making changes to the data you're working with, you will need to stop the applications, services, and jobs running against the database while the restore takes place.
